---
title: "Monado - Using libsurvive"
layout: main
---

# The libsurvive driver in Monado

Before enabling the libsurvive driver in Monado, libsurvive has to be installed as a library before compiling monado. For example with cmake and ninja
```
git clone https://github.com/cntools/libsurvive.git
cd libsurvive
cmake -DCMAKE_INSTALL_PREFIX=/usr -G Ninja -DCMAKE_BUILD_TYPE=Release -Bbuild
ninja -C build install
```

To check whether Monado was built with libsurvive support, run `monado-cli probe` and check whether it appears in the list of `Built in drivers`.

When starting any libsurvive or OpenXR application, libsurvive will use cached calibration in `$XDG_CONFIG_HOME/libsurvive/config.json`, or `~/.config/libsurvive/config.json` or run a fresh calibration if this config does not exist yet.

The device the calibration is performed on should not be moved during calibration. Calibration and running with libsurvive may work best when as many device sensors as possible can "see" both basestations at the same time, and when the distance to the basestations is at least 0.5-1 meter.

Note that monado does not pass parameters to libsurvive. For example to force calibration with a specific device use the parameter `--requiredtrackersforcal` with any of the libsurvive tools.

An example to calibrate using a Valve Index HMD, would be `survive-cli --requiredtrackersforcal T20 --allowalltrackersforcal 0`.

To remove libsurvive's calibration data (e.g. to force recalibration or after a significant libsurvive update) delete the configuration file:

    rm ~/.config/libsurvive/config.json

